import java.util.ArrayList;

import com.devcamp.j04_javabasic.s10.CCat;
import com.devcamp.j04_javabasic.s10.CDog;
import com.devcamp.j04_javabasic.s10.CPerson;
import com.devcamp.j04_javabasic.s10.CPet;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Subtask 5 đây thầy ơi:");
        CPerson subtask5 = new CPerson();

        ArrayList<CPet> petList = new ArrayList<>();
        CPet conMeo = new CCat();
        CPet conCho = new CDog();
        petList.add(conCho);
        petList.add(conMeo);
        subtask5.setPets(petList);
        System.out.println(subtask5.toString());

        // Subtask 6:
        subtask5.setFirstName("Subtask 6 đây thầy ơi!");
        System.out.println(subtask5.getFirstName());
    }
}
